# Project 7: Brevet time calculator with API functionality And Authentication

This is exactly like project 4 or 5, its a brevet calcaulator that uses Flask on the server, and Javascript and AJAX on the client.
This version also includes API functionality

Credit to UO CIS Department for the orignial program
New and updated functionality by Kendell Crocker kendellc@uoregon.edu

## What is in this repository

This is the source code for the program. Using your verisoning software of choice, download the contents of this repoistiory, and "run docker-compose up" (More detailed instructions below)
Note: Recommend using GIT, but other versioning software will work

## How to setup

You need docker. Below is a link to instructions on how to download and install docker for your machine:

https://docs.docker.com/install/

Also, you need docker-compose. Here's another link for that:

https://docs.docker.com/compose/install/

PLEASE NOTE THAT DOCKER ON WINDOWS DOES NOT ALWAYS FUNCTION AS EXPECTED. IT IS HIGHLY RECOMMENDED THAT YOU USE MAC OS OR A LINUX DISTRO OF YOUR CHOICE FOR THIS.

Once the program is up and running, you can do two things
Basic program: navigate to http://<host:port> (default is 5000) and you can use the basic program
APIs: navigate to http://<host:port> (default is 5001) and you can use the API calls (See Below)

Another note: Had trouble getting each container to link to mongoDB, the IP address kept changing. (See piazza thread "Cant connect to database") There is an option in credentials.ini to manually tell the other containers where mongoDB is. Hopefully you won't need to change this option.

## API Calls
Valid API calls:
*http://<host:port>/listAll
	*returns a list of all open and close times in a JSON format

*http://<host:port>/listOpenOnly
	*returns a list of all open times in a JSON format

*http://<host:port>/listCloseOnly
	*returns a list of all close times in a JSON format

*http://<host:port>/listAll/csv
	*returns a list of all open and close times in a CSV (text) format

*http://<host:port>/listOpenOnly/csv
	*returns a list of all open times in a CSV format

*http://<host:port>/listCloseOnly/csv
	*returns a list of all close times in a CSV format

*http://<host:port>/listAll/json
	*returns a list of all open and close times in JSON format

*http://<host:port>/listOpenOnly/json
	*returns a list of all open times in a JSON format

*http://<host:port>/listCloseOnly/json
	*returns a list of all close times in a JSON format

Also, you can specify a format and how many times you want to see by adding "?top='x'" to your request. Examples below

*http://<host:port>/listOpenOnly/csv?top=5
	*returns a list of the first 5 open times in csv format

*http://<host:port>/listCloseOnly/json?top=3
	*returns a list of the first 3 close times in JSON format
    
*http://<host:port>/api/register?user=<your-user-name>&password=<your-password>
    *adds a new user to the database with password. Also gives it a unique internal ID

*http://<host:port>/api/token?user=<your-user-name>&password=<your-password>
    *provide your username and password, and a token will be given to you. It will only last for 10 minutes.